﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {
    public Text text;
    float time = 0;
    String formatedTime;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        time += Time.deltaTime;
        formatedTime = time.ToString("F0");
        text.text = "Time: " + formatedTime;
	}
}
